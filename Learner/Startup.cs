﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Learner.Startup))]
namespace Learner
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
